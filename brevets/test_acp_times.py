from acp_times import open_time, close_time
import arrow

"""
*Note: The offset time differences are handled in the calc.html file.
       So I wrote test cases specific to how I coded this project.
"""

def test_open_with_Zerovalue():
    """
    This will test a control of distance 0. Return
    value should just be the users input start time 
    and date. (No errors).
    """
    time = '2017-01-01 08:00' 
    temp = arrow.get(time)
    assert str(open_time(0, 400, temp)) == temp.isoformat()


def test_improperClose_200km():
    """
    By the rules, the overall time limit for a 200km brevet is 13H30,
    even though by calculation, 200/15 = 13H20

    * In this specific test, I had to increment the hours by 12 rather
      than 13 because calc.html file handles the 1 hour offset. However,
      you'll notice that the 20 minutes reflects the minutes from the 
      computation 200/15.
    """
    time = '2017-01-01 08:00'
    temp = arrow.get(time)
    result = temp.shift(hours=+12, minutes=+20)
    assert str(close_time(200, 200, temp)) != result.isoformat()

def test_properClose_200km():
    """
    By the rules, the overall time limit for a 200km brevet is 13H30,
    even though by calculation, 200/15 = 13H20
 
    * In this specific test, I had to increment the hours by 12 rather than
      13 because the calc.html file handles the 1 hour offset. Again, you'll
      notice that the 30 minutes reflects the minutes from rule (i.e., 30 min). 
    """
    time = '2017-01-01 08:00'
    temp = arrow.get(time)
    result = temp.shift(hours=+12, minutes=+30)
    assert str(close_time(200, 200, temp)) == result.isoformat()

def test_under_60km():
    """
    This will test the open time for a control input
    that's under 60km. As per ACP, the first 60km will
    follow the rule of 20km/hr. 
    """
    time = '2017-01-01 08:00'
    temp = arrow.get(time)
    result = temp.shift(hours=+2, minutes=+30)
    assert str(close_time(50, 200, temp)) == result.isoformat()

def test_string():
    """
    This will test if a string is given as a control
    rather than mile/km. Output will be the 0km start
    and close time. No error.
    """
    time = '2017-01-01 08:00'
    temp = arrow.get(time)
    assert str(open_time("blahblah", 600, temp)) == temp.isoformat()

