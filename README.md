# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

This software mimics the behavior of the calculator as shown here (https://rusa.org/octime_acp.html)


## ACP controle times

Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   


## AJAX and Flask reimplementation

Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax. Open and close times are calculated by the French algorithm given here (https://ruse.org/octime_acp.html). Provided is a concrete description of their algorithm, as well as specific anonymous cases we should be aware about.


## Testing

A suite of automated nose test cases were created in order to test the functionality of the software. The test cases were written to reflect the rules from (https://rusa.org/pages/acp-brevet-control-times-calculator). They also target a few specific tests cases that were mentioned in the rusa site, and some that were not. 


## Tasks

* Mimic the proper behavior for capturing user input via Ajax (front-end)

* Modify the acp_times.py file so that it provides the proper logic in order to calculate the corresponding open and close brevet times. 

* Implement the nose module by automating a few test cases, and testing the logic written from acp_times.py


# Contributor
-------------

Ronny Fuentes <ronnyf@uoregon.edu>
